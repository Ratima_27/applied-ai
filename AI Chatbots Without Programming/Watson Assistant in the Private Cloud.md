Due to regulations concerning data handling, some organization may require their chatbot to be available on premises (in a private rather than public cloud).

Watson Assistant (and other IBM Cloud services) are now available on premises as well.

If your company or a client of yours has such requirements, consider reading the [ official announcement here](https://www.ibm.com/blogs/watson/2018/10/watson-services-now-available-on-premises/).

As you know, in this course, we use IBM Watson Assistant (formerly Conversation) to teach you how to build chatbots.

Alternatives from Amazon, Google, Microsoft, and others, do exist. The reason why we opted for Watson Assistant is not just our affiliation with IBM. It's because it's the best tool for the job.

Don't take our word for it. Market research company Forrester analyzed the conversational computing platforms of seven major providers and declared, "[ IBM Leads The Pack](https://reprints.forrester.com/#/assets/2/73/RES137816/reports)".

Watson Assistant is a truly remarkable piece of technology that will allow anyone, not just programmers, to build smart chatbots and conversational interfaces.

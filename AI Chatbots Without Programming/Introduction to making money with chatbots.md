The demand for chatbots will likely skyrocket in the next few years. Businesses are enthralled by the possibility of saving costs and providing a true 24/7 customer service offer. So much so, that Gartner predicted:

By 2020, customers will manage 85% of their relationship with the enterprise without interacting with a human.

That means chatbots and other AI-powered/automated ways. Who is going to build such chatbots? Perhaps you!

As you can imagine, this puts you in a unique position to profit from this need and interest for chatbots.

There are two main approaches you could take:

Builds chatbots for clients;
Create sites for clients, which include chatbots.
The latter will definitely appeal to entrepreneurs who already build sites for clients or at least have the skills to provide a complete solution, including creating custom sites (say, powered by WordPress).

Whichever option appeals the most to you, note that you don't need to already have a company in order to provide these services. This could be your opportunity to start your own business. You can start small, with no risks, and grow the business from there. There are no real investments to be made here.

In fact, in the next lab, you'll be able to [enroll](https://www.ibm.com/account/reg/us-en/signup?formid=urx-30677&src=CC_edX_CB0103EN_Freemium_AIBot250&cm_mmc=Email_External-_-Developer_Ed+Tech-_-WW_WW-_-Campaign-Cognitive+Class+CB0103EN-Courseware&cm_mmca1=000026UJ&cm_mmca2=10006555&cm_mmca3=M00000001&cvosrc=email.External.M00000001&cvo_campaign=000026UJ) as an IBM partner and receive a generous amount of Watson Assistant credits for a year, for free. This gives you credibility as you approach clients, as well as the ability to create several chatbots for free for the first year.

Make sure you don't skip the lab in the next section. See you there. 

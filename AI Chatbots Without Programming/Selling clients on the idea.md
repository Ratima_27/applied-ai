After completing this course, you'll have the skills to create Frequently Asked Questions and other basic customer service chatbots. That's great, but how do you sell clients on the idea?

###  Selling clients on chatbots
If you already have clients, then your job is greatly simplified, as this will be just another service you offer. All you'll need to do is inform them about this new chatbot building service you now offer. Whether they are new or existing clients, however, you'll need to convince them that chatbots are a worthwhile investment.

To help you sell your clients on the idea, I would recommend preparing a presentation showing the benefits of chatbots. I'd structure it into the following three sections:

1. Chatbots are trending: This is where you show how much buzz there is around chatbots. Include the Gartner quote and others you can easily find by searching on Google. Mention how much interest other businesses have shown. 
2. What chatbots can do for your business: This is your value proposition. Illustrate how the client's company will save (or make money) from having a chatbot. Stress 24/7 availability and the fact that chatbots can free customer service resources by acting as a filter for common inquiries from their customers.
3. My pricing structure: Here is the part where you sell your services and talk about money. You can mention that your chatbot will be powered by IBM Watson and that you are a partner. When discussing fees, you'll want to explain the initial cost and monthly maintenance fees.

###   What to charge
How you price your services is up to you. It will depend on your specific clients and their needs, the competition you face, your skill level, whether you sell in person or online, your country, and more.

If you are uncertain, set an hourly fee for yourself and ballpark from there. If you set your hourly rate to $50 USD, for example, and you estimate that a chatbot for a specific customer will take you 10 hours to build, you could quote them (and then bill them) for a $500 initial fee plus a monthly maintenance fee. 

You'll want to bill them a maintenance fee so as to cover your IBM Cloud service costs (once you no longer get them for free) and the time you'll need to spend to perform the occasional update to the chatbot. You could make the maintenance fee conditional on usage (e.g., a higher fee if they go over a certain amount of API calls).

As you'll learn in the last module of this course, if you are deploying the chatbot on a WordPress website, you'll even be able to set a limit to the chatbot usage (just explain to the customer that the chatbot won't be shown to customers once the API limit you agreed upon has been reached).

It's worth noting that you can, but you are under no obligation to share with your clients that IBM is giving you the first year of resources for free.

After you've been at this for a while, you can show prospective clients chatbots you'll have built for other clients. This will also allow you to include testimonials in your presentation. If you have them, testimonial quotes about cost-saving (or profit-making) are always appealing to businesses.
